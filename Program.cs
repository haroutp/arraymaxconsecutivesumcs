﻿using System;

namespace ArrayMaxConsecutiveSum
{
    class Program
    {
        static void Main(string[] args)
        {

        }
        int arrayMaxConsecutiveSum(int[] inputArray, int k) {
            int maxSum;
            int current = 0;
            for(int i = 0; i < k; i++)
                current += inputArray[i];
            
            maxSum = current;
            for(int i = k; i < inputArray.Length; i++){
                current += inputArray[i] - inputArray[i - k];
                if(current > maxSum){
                    maxSum = current;
                }
            }

            return maxSum;
        
        }
    }
}
